package testes

import (
	"testing"
)

type ProtoString struct {
	Value string `protobuf:"bytes,2,opt,name=Value,proto3" json:"Value,omitempty"`
}

type ModelString struct {
	Value string `json:"value"`
}

type ProtoInt struct {
	Value int64 `protobuf:"varint,1,opt,name=Value,proto3" json:"Value,omitempty"`
}

type ModelInt struct {
	Value int `json:"value"`
}

type ProtoFloat struct {
	Value float32 `protobuf:"fixed32,1,opt,name=Value,proto3" json:"Value,omitempty"`
}

type ModelFloat struct {
	Value float32 `json:"value"`
}

func TestCreateConn(t *testing.T) {
	x := common.CreateConn("localhost")
	if x == nil {
		t.Error("Erro em criar connexão")
	}
}

func TestModelToProtoString(t *testing.T) {
	modelString := &ModelString{
		Value: "valueTeste",
	}

	var protoString *ProtoString = common.ModelToProto(&ProtoString{}, modelString).(*ProtoString)

	if protoString.Value != "valueTeste" {
		t.Error("Erro em resolver ModelToProto")
	}
}

func TestProtoToModelString(t *testing.T) {
	protoString := &ProtoString{
		Value: "valueTeste",
	}

	var modelString *ModelString = common.ProtoToModel(&ModelString{}, protoString).(*ModelString)

	if modelString.Value != "valueTeste" {
		t.Error("Erro em resolver ProtoToModel")
	}
}

func TestModelToProtoInt(t *testing.T) {
	modelInt := &ModelInt{
		Value: 10,
	}

	var protoInt *ProtoInt = common.ModelToProto(&ProtoInt{}, modelInt).(*ProtoInt)

	if protoInt.Value != 10 {
		t.Error("Erro em resolver ModelToProto")
	}
}

func TestProtoToModelInt(t *testing.T) {
	protoInt := &ProtoInt{
		Value: 10,
	}

	var modelInt *ModelInt = common.ProtoToModel(&ModelInt{}, protoInt).(*ModelInt)

	if modelInt.Value != 10 {
		t.Error("Erro em resolver ProtoToModel")
	}
}

func TestModelToProtoFloat(t *testing.T) {
	usuarioModel := &ModelFloat{
		Value: 10.3,
	}

	var protoFloat *ProtoFloat = common.ModelToProto(&ProtoFloat{}, usuarioModel).(*ProtoFloat)

	if protoFloat.Value != 10.3 {
		t.Error("Erro em resolver ModelToProto")
	}
}

func TestProtoToModelFloat(t *testing.T) {
	protoFloat := &ProtoFloat{
		Value: 10,
	}

	var modelFloat *ModelFloat = common.ProtoToModel(&ModelFloat{}, protoFloat).(*ModelFloat)

	if modelFloat.Value != 10 {
		t.Error("Erro em resolver ProtoToModel")
	}
}
