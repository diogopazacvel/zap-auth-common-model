package models

type Application struct {
	ID          string `json:"id"`
	Name        string `json:"name" validate:"required,min=1,max=256"`
	Description string `json:"description" validate:"required,min=10,max=256"`
}

type ApplicationPaged struct {
	Limit  int           `json:"limit"`
	Offset int           `json:"offset"`
	Size   int           `json:"size"`
	Items  []Application `json:"items"`
}
