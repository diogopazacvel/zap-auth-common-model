package models

type AccountRole struct {
	ID        string `json:"id"`
	AccountID string `json:"accountId" validate:"required"`
	RoleID    string `json:"roleId" validate:"required"`
}

type AccountRoleVO struct {
	ID            string `json:"id"`
	AccountID     string `json:"accountId"`
	RoleID        string `json:"roleId"`
	NameRole      string `json:"nameRole"`
	ApplicationID string `json:"applicationId"`
}
