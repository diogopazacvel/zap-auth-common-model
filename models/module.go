package models

type Module struct {
	ID            string `json:"id"`
	Name          string `json:"name" validate:"required,min=1,max=256"`
	ApplicationID string `json:"applicationId" validate:"required"`
}

type ModulePaged struct {
	Limit  int      `json:"limit"`
	Offset int      `json:"offset"`
	Size   int      `json:"size"`
	Items  []Module `json:"items"`
}
