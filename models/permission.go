package models

type Permission struct {
	ID          string `json:"id"`
	Name        string `json:"name" validate:"required,min=2,max=256"`
	Description string `json:"description" validate:"required,min=2,max=256"`
	ModuleID    string `json:"applicationId" validate:"required"`
}

type PermissionInput struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ModuleID    string `json:"moduleId"`
}

type PermissionPaged struct {
	Limit  int          `json:"limit"`
	Offset int          `json:"offset"`
	Size   int          `json:"size"`
	Items  []Permission `json:"items"`
}

type PermissionVO struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Bound       bool   `json:"bound"`
	ModuleID    string `json:"moduleId"`
}
