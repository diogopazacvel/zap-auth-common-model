package models

type AccountPermission struct {
	ID           string `json:"id"`
	AccountID    string `json:"accountId" validate:"required"`
	PermissionID string `json:"permissionId" validate:"required"`
}
