package models

type Session struct {
	ID            string `json:"id" validate:"required"`
	Token         string `json:"token" validate:"required"`
	AccountID     string `json:"accountId" validate:"required"`
	DataLogin     string `json:"dataLogin" validate:"required"`
	IpSession     string `json:"ipSession" validate:"required"`
	ApplicationID string `json:"applicationId" validate:"required"`
}
