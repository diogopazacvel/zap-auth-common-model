package models

type RolePermission struct {
	ID           string `json:"id"`
	RoleID       string `json:"roleId" validate:"required"`
	PermissionID string `json:"permissionId" validate:"required"`
}
