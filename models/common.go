package models

type InputPaged struct {
	Limit   int    `json:"limit"`
	Offset  int    `json:"offset"`
	Filters string `json:"filters"`
	Params  string `json:"params"`
	OrderBy string `json:"orderBy"`
}

type CommonResponse struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}
