package models

type Role struct {
	ID            string `json:"id"`
	Name          string `json:"name" validate:"required,min=2,max=256"`
	ApplicationID string `json:"applicationId" validate:"required"`
}

type RolePaged struct {
	Limit  int    `json:"limit"`
	Offset int    `json:"offset"`
	Size   int    `json:"size"`
	Items  []Role `json:"items"`
}
