package models

type Account struct {
	ID        string `json:"id"`
	Email     string `json:"email" validate:"required,email"`
	Password  string `json:"password"`
	Name      string `json:"name" validate:"required,min=1,max=200"`
	Telephone string `json:"telephone" validate:"required,min=1,max=14"`
}

type AccountPaged struct {
	Limit  int       `json:"limit"`
	Offset int       `json:"offset"`
	Size   int       `json:"size"`
	Items  []Account `json:"items"`
}
