package models

type AccountLogin struct {
	ID       string `json:"id"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=6,max=200"`
	Name     string `json:"name" validate:"required,min=1,max=200"`
}
