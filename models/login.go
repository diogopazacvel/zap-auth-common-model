package models

type LoginInput struct {
	Email         string `json:"email"`
	Password      string `json:"password"`
	ApplicationID string `json:"applicationId"`
}

type LoginResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
}
