CREATE TABLE account(
   id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
   email VARCHAR NOT NULL,
   password VARCHAR NOT NULL,
   name VARCHAR NOT NULL,
   telephone VARCHAR NOT NULL
);

CREATE TABLE application(
   id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
   name VARCHAR NOT NULL,
   description VARCHAR NOT NULL
);

CREATE TABLE account_login(
    id UUID PRIMARY KEY,
    email VARCHAR NOT NULL,
	password VARCHAR NOT NULL,
	name VARCHAR NOT NULL
);

CREATE TABLE role(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR NOT NULL,
    application_id UUID NOT NULL
);

CREATE TABLE permission(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    module_id UUID NOT NULL
);

CREATE TABLE session(
   id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
   token VARCHAR NOT NULL,
   account_id UUID NOT NULL,
   application_id UUID NOT NULL
);

CREATE TABLE role_permission(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    role_id UUID NOT NULL,
    permission_id UUID NOT NULL
);

CREATE TABLE account_role(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    account_id UUID NOT NULL,
    role_id UUID NOT NULL
);

CREATE TABLE account_permission(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    account_id UUID NOT NULL,
    permission_id UUID NOT NULL
);

CREATE TABLE module(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR NOT NULL,
    application_id UUID NOT NULL
);

ALTER TABLE role
ADD CONSTRAINT fk_role_application FOREIGN KEY (application_id) 
REFERENCES application(id);

ALTER TABLE permission
ADD CONSTRAINT fk_permission_module FOREIGN KEY (module_id) 
REFERENCES module(id);

ALTER TABLE session
ADD CONSTRAINT fk_session_account_login FOREIGN KEY (account_id) 
REFERENCES account_login(id);

ALTER TABLE session
ADD CONSTRAINT fk_session_application FOREIGN KEY (application_id) 
REFERENCES application(id);

ALTER TABLE role_permission
ADD CONSTRAINT fk_role_permission_role FOREIGN KEY (role_id) 
REFERENCES role(id);

ALTER TABLE role_permission
ADD CONSTRAINT fk_role_permission_permission FOREIGN KEY (permission_id) 
REFERENCES permission(id);

ALTER TABLE account_permission
ADD CONSTRAINT fk_account_permission_permission FOREIGN KEY (permission_id) 
REFERENCES permission(id);

ALTER TABLE account_permission
ADD CONSTRAINT fk_account_permission_account_login FOREIGN KEY (account_id) 
REFERENCES account_login(id);

ALTER TABLE account_role
ADD CONSTRAINT fk_account_role_role FOREIGN KEY (role_id) 
REFERENCES role(id);

ALTER TABLE account_role
ADD CONSTRAINT fk_account_role_account_login FOREIGN KEY (account_id) 
REFERENCES account_login(id);

ALTER TABLE module
ADD CONSTRAINT fk_module_application FOREIGN KEY (application_id) 
REFERENCES application(id);